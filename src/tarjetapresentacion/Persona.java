package tarjetapresentacion;
import java.io.BufferedReader;
import java.io.FileReader;
/**
 *
 * @author Aseret Guzmán Luna
 */
public class Persona {
    private String nombre;
    private short  edad;
    private char sexo;
    private String nacionalidad;
    private String ocupacion;
    private int telefono;
    private long celular;
    private String correo;
    
    Persona(String archivo){
        Leer(archivo);
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int teléfono) {
        this.telefono = teléfono;
    }

    public long getCelular() {
        return celular;
    }

    public void setCelular(long celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }
    
    private void Leer(String archivo){
        FileReader fr = null;
        BufferedReader br = null;
        try{
            fr= new FileReader(archivo);
            br = new BufferedReader (fr);
            
            String nombre = br.readLine();
            this.nombre = nombre;
            String edad = br.readLine();
            this.edad = Short.parseShort(edad);
            String temp = br.readLine().substring(0, 1).toLowerCase();
            if(temp.equals("h")||temp.equals("m")){
                this.sexo = temp.charAt(0);
            }else{
                this.sexo = ' ';
            }
            String nacionalidad = br.readLine();
            this.nacionalidad = nacionalidad;
            String ocupacion = br.readLine();
            this.ocupacion = ocupacion;
            String telefono = br.readLine().trim();
            this.telefono = Integer.parseInt(telefono);
            String celular = br.readLine().trim();
            this.celular = Long.parseLong(celular);
            String correo = br.readLine();
            this.correo = correo;
        }catch(Exception e){
            System.err.println("Error con el archivo");
            e.printStackTrace();
            System.exit(0); 
        }finally{
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            System.err.println("Error con el archivo!");
            e2.printStackTrace(); 
            System.exit(0);
         }
        }    
    }
    public void mostrar(){
        System.out.println("|---------------------------------------------------------|");
        System.out.println("| Nombre: "+getNombre()+"                              |");
        System.out.println("|---------------------------------------------------------|");
        System.out.println("| Nacionalidad: "+getNacionalidad()+"                                  |");
        System.out.println("|---------------------------------------------------------|");
        if(getSexo() != 'h') {
            System.out.println("| Sexo: Mujer                Edad: "+getEdad()+" años                |");
        } else {
            System.out.println("| Sexo: Hombre               Edad: "+getEdad()+" años                 |");
        }
        System.out.println("|---------------------------------------------------------|");
        System.out.println("| Ocupación: "+getOcupacion()+"                       |");
        System.out.println("|---------------------------------------------------------|");
        System.out.println("| Telefono Fijo: "+getTelefono()+"                                 |");
        System.out.println("|---------------------------------------------------------|");
        System.out.println("| Celular: "+getCelular()+"                                     |");
        System.out.println("|---------------------------------------------------------|");
        System.out.println("| Email: "+getCorreo()+"                |");
        System.out.println("|---------------------------------------------------------|");
    }
    
}
